Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pagure
Source: https://pagure.io/pagure
Files-Excluded:
 pagure/static/vendor/bootstrap/*
 pagure/static/vendor/cal-heatmap/*.min.js
 pagure/static/vendor/codemirror/*
 pagure/static/vendor/d3/*
 pagure/static/vendor/diff2html/*.min.js
 pagure/static/vendor/diff2html/*.min.css
 pagure/static/vendor/emojione/*.min.js
 pagure/static/vendor/emojione/*.min*.css
 pagure/static/vendor/font-awesome/*
 pagure/static/vendor/highlight.js/*
 pagure/static/vendor/highlightjs-line-numbers/*
 pagure/static/vendor/jquery/*
 pagure/static/vendor/jquery.atwho/*
 pagure/static/vendor/jquery.caret/*
 pagure/static/vendor/jquery.textcomplete/*
 pagure/static/vendor/jstimezonedetect/*
 pagure/static/vendor/lazyload/*
 pagure/static/vendor/selectize/*
 pagure/hooks/files/default_hook.py
 pagure/hooks/files/git_multimail.py
 pagure/hooks/files/mirror.py
 pagure/hooks/files/pagure_block_unsigned.py
 pagure/hooks/files/pagure_force_commit_hook.py
 pagure/hooks/files/pagure_hook.py
 pagure/hooks/files/pagure_hook_requests.py
 pagure/hooks/files/pagure_hook_tickets.py
 pagure/hooks/files/pagure_no_new_branches
 pagure/hooks/files/rtd_hook.py
 pagure/themes/pagureio/*
 pagure/themes/srcfpo/*
 pagure/themes/chameleon/static/fonts/OpenSans-*.ttf
 doc/_build/*

Files: *
Copyright: 2014-2018 Red Hat, Inc
License: GPL-2+

Files: pagure/hooks/files/git_multimail_upstream.py
Copyright: 2015-2016 Matthieu Moy and others
           2012-2014 Michael Haggerty and others
           2007 Andy Parkins
License: GPL-2

Files: pagure/static/toggle.css
Copyright: 2012-2013 Thibaut Courouble
License: Expat

Files: debian/*
Copyright: 2019 Sergio Durigan Junior <sergiodj@debian.org>
License: GPL-2+

License: GPL-2
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining
 a copy of this software and associated documentation files (the
 "Software"), to deal in the Software without restriction, including
 without limitation the rights to use, copy, modify, merge, publish,
 distribute, sublicense, and/or sell copies of the Software, and to
 permit persons to whom the Software is furnished to do so, subject to
 the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
